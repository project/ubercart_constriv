<?php

/*
 * GestPayCrypt e GestPayCryptHS 2.0.1
 * Copyright (C) 2001-2004 Alessandro Astarita <aleast@capri.it>
 *
 * http://gestpaycryptphp.sourceforge.net/
 *
 * GestPayCrypt-PHP is an implementation in PHP of GestPayCrypt e
 * GestPayCryptHS italian bank Banca Sella java classes. It allows to
 * connect to online credit card payment GestPay.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details at 
 * http://www.gnu.org/copyleft/lgpl.html
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */


class GestPayCrypt {
	// Public
	var $URL;				// URL per connessione
	var $ShopLogin;         // Shop Login che identifica l'esercente
	var $ShopPassword; 		// Password 
	var $Currency;          // Codice che identifica la divisa in cui e' denominato l'importo
	var $Amount;            // Importo della transazione
	var $Action;			// 1=Purchase ; 4=Authorization ;
	var $ShopTransactionID; // Identificativo attribuito alla transazione dall'esercente
	var $ResponseURL;       // call back per esito
  var $ErrorURL;
	var $Language;          // Lingua selezionata
	var $UDF3;				// trasmette indirizzo email alla HPP
	var $UDF4;				// trasmette un valore che se viene restituito vuol dire che la transazione è legittima
	var $UDF5;				// trasmette un valore di timeout
	var $Response;			// Risposta dal server (Payementid:URL);
	
	function GestPayCrypt()
	{
		$this->URL = "";
		$this->ShopLogin = "";
		$this->ShopPassword = "";
		$this->Currency = "";
		$this->Amount = "";
		$this->Action = ""; 
		$this->ShopTransactionID = "";
		$this->ResponseURL ="";
    $this->ErrorUrl = "";
		$this->Language = "";
		$this->UDF3 = "";
		$this->UDF5 = "";
		$this->UDF5 = "";
		$this->Response = "";
	
		
	}

	// Public

	// Metodi di valorizzazione attributi
	function SetURL($val)
	{
		$this->URL = $val;
	}
	function SetShopLogin($val)
	{
		$this->ShopLogin = $val;
	}
	
	function SetShopPassword($val)
	{
		$this->ShopPassword = $val;
	}
	
	function SetAction($val)
	{
		$this->Action = $val;
	}
	
	function SetDomain($val)
	{
		$this->DomainName = $val;
	}

	function SetCurrency($val)
	{
		$this->Currency = $val;
	}

	function SetAmount($val)
	{
		$this->Amount = $val;
	}

	function SetShopTransactionID($val)
	{
		$this->ShopTransactionID = urlencode(trim($val));
	}

	function SetBuyerEmail($val)
	{
		$this->BuyerEmail = trim($val);
	}

	function SetLanguage($val)
	{
		$this->Language = trim($val);
	}

	function SetResponseUrl($val)
	{
		$this->ResponseURL = $val;
	}
  
  function SetErrorUrl($val) {
    $this->ErrorURL = $val;
  }
	
	
	function SetUDF3($val)
	{
		$this->UDF3 = $val;
	}
	
	function SetUDF4($val)
	{
		$this->UDF4 = $val;
	}
	
	function SetUDF5($val)
	{
		$this->UDF5 = $val;
	}
	
	// Metodi di lettura attributi
	function GetShopLogin()
	{
		return $this->ShopLogin;
	}

	function GetCurrency()
	{
		return $this->Currency;
	}

	function GetAmount()
	{
		return $this->Amount;
	}

	function GetCountry()
	{
		return $this->country;
	}

	function GetShopTransactionID()
	{
		return urldecode($this->ShopTransactionID);
	}

	function GetBuyerEmail()
	{
		return $this->BuyerEmail;
	}

	
	function GetResponse()
	{
		return $this->Response;
	}
	function Connect()
	{
		// collegamento al MPS
		$DataToSend = "id=$ID&password=$Password&action=$Action&amt=$Amt&currencycode=978&langid=ITA&responseURL=$ResponseURL
		&errorURL=$ErrorURL&trackid=$TrackId&udf1=AA&udf2=BB&udf3=EMAILADDR:" . $emailaddress . "&udf4=DD&udf5=EE"; //udf= EMAILADDR:indirizzo email per ricevuta di ritorno
 
 		$DataToSend = "id=" . $this->ShopLogin .
              "&password=" . $this->ShopPassword .
              "&action=". $this->Action .
              "&amt=" . $this->Amount .
              "&currencycode=". $this->Currency .
              "&langid=" . $this->Language .
              "&responseURL=".  $this->ResponseURL .
              "&errorURL=" . $this->ErrorURL .
              "&trackid=" . $this->ShopTransactionID .
              "&udf1=SHA1&udf2=AA&udf3=EMAILADDR:" .$this->UDF3 .
              "&udf4=" . $this->UDF4 .
              "&udf5=" . $this->UDF5;
		$ch=curl_init($this->URL); 
 
		//Imposta gli headers HTTP  + curl per protocollo https
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch,CURLOPT_POST,1);
	
		//Invia i dati
		curl_setopt($ch,CURLOPT_POSTFIELDS,$DataToSend);
		
		//imposta la variabile PHP  
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1); 
		
		//Riceve la risposta dal server
		// $temp = 
		watchdog('constriv', "Collegamento al server, dati inviati <br> $DataToSend", array(), WATCHDOG_DEBUG);
		$this->Response = curl_exec($ch);
		watchdog('constriv', "Collegamento al server, risposta ". $this->Response, array(), WATCHDOG_DEBUG);
		//chiude la connessione 
		curl_close($ch);  			  
		// per testare, usare i seguenti numeri di carta con qualsiasi data futura e CVV
		//cc# 4539990000000012 , esito positivo
		//cc# 4539990000000020 , esito negativo 
		//cc# 4999000055550000 , non elaborata per dati non corretti   
	}

}



?>
