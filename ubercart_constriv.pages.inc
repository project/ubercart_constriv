<?php

/**
 * @file
 * ubercart_constriv menu pages
 *
 */

 /**
  * Page for payment complete process: cart/constriv/%/complete/%
  * We get redirected here from server responseURL after the NotificationMessage
  * 
  */
function ubercart_constriv_redirect_complete($order_id, $hashid) {
  

  
  $order = uc_order_load($order_id);
  if ($order === FALSE) {
    watchdog('constriv', 'Error in CompleteURL for order #!orderid: wrong ORDER status.', array('!orderid' => $order_id), WATCHDOG_ERROR);
    drupal_set_message(t('An error has occurred during payment process. Please contact us to ensure your order has been submitted.'), 'error');
    drupal_goto('cart');
  }

  // Calculating the expected HASH-ID for this order
  $my_hashid = ubercart_constriv_get_hashid($order->order_id .':'. $order->created); 
  
  if ($my_hashid != $hashid) {
    watchdog('constriv', 'Error in CompleteURL for order #!orderid: unmatching HASH-ID received.', array('!orderid' => $order_id), WATCHDOG_ERROR);
    drupal_set_message(t('An error has occurred during payment process. Please contact us to ensure your order has been submitted.'), 'error');
    drupal_goto('cart');
  }
  
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  $page = variable_get('uc_cart_checkout_complete_page', '');

  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}

/**
 * Page for the ResponseURL invocation: cart/constriv/%/response
 * This page is invocated back from the Consorzio Triveneto server, to request the final user redirect
 * after payment process is completed. Response to the NotificationMessage
 * Data is sent to this page using POST
 */
function ubercart_constriv_redirect_response($order_id) {
  
  $data = _ubercart_constriv_parse_server_data($_POST);

  // Calculating the expected HASH-ID for this order
  $hashid = ubercart_constriv_get_hashid($order_id); 
  
  if ($order_id != $data['trackid'] || $data['udf4'] != $hashid) {
    watchdog('constriv', 'Error in ResponseURL for order #!orderid: unmatching HASH-ID received.', array('!orderid' => $order_id), WATCHDOG_ERROR);
    die('ERROR: Wrong arguments (HASH).');
  }
  
  $order = uc_order_load($order_id);
  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    watchdog('constriv', 'Error in ResponseURL for order #!orderid: wrong ORDER status.', array('!orderid' => $order_id), WATCHDOG_ERROR);
    die('ERROR: Wrong arguments (OID).');
  }
  
  $url = '';
  // If the payment is OK, redirect to the complete page process
  if ($data['responsecode'] == '00' && ($data['result'] == 'APPROVED' || $data['result'] == 'CAPTURED')) {
    // Process the payment process here, set the order as PAYED

    
    // User changed the eMail
    $mail = strtolower(str_replace('EMAILADDR:', '', $data['udf3']));    
    if (strcasecmp($mail, $order->primary_email) !== 0) {
      uc_order_comment_save($order->order_id, 0, t('Customer used a different e-mail address during payment: !email', array('!email' => $mail)), 'admin');
    }
    
    $comment = t('Payment approved by Constriv (TransactionID #!tranid; Auth-Code !auth).',
      array('!tranid' => $data['tranid'], '!auth' => $data['auth']));

    // Entering the Order Payment using uc_payement_enter() funcion, see http://api.lullabot.com/uc_payment_enter
    uc_payment_enter($order->order_id, 'constriv', $order->order_total, 0, $data,
      t('Payment approved by Constriv (TransactionID #!tranid; Auth-Code !auth).', array('!tranid' => $data['tranid'], '!auth' => $data['auth']))
    );
    
    uc_order_comment_save($order->order_id, 0,
      t('Payment approved by Constriv (TransactionID #!tranid; Auth-Code !auth).', array('!tranid' => $data['tranid'], '!auth' => $data['auth'])),
      'admin'
    );
    
    watchdog('constriv', 'Payment Completed for order #!orderid: TransactionID #!tranid; Auth-Code !auth.',
      array('!orderid' => $order->order_id, '!tranid' => $data['tranid'], '!auth' => $data['auth']), WATCHDOG_NOTICE
    );
    
    $hashid = ubercart_constriv_get_hashid($order->order_id .':'. $order->created);
    // Set the redirect URL
    $url = url('cart/constriv/'.$order_id.'/complete/' . $hashid, array('absolute' => TRUE));
  }
  else {
    // Payment error
    
    // Technical errors?
    if (!empty($data['error'])) {
      $data['responsecode'] = $data['error'];
    }
    else {
      // @TODO: fill with ConsTriv descriptions on XX codes...
      $data['errortext'] = _ubercart_constriv_get_server_error_codes($data['error']);
    }
    
    watchdog('constriv', 'Error in payment process for order #!orderid: result code = !responsecode, message="!errortext".',
      array('!orderid' => $order_id, '!responsecode' => $data['responsecode'], '!errortext' => $data['errortext']), WATCHDOG_ERROR
    );
    uc_order_comment_save($order_id, 0, t('Error: Payment not correct, error=!responsecode, message="!errortext"',
      array('!responsecode' => $data['responsecode'], '!errortext' => $data['errortext'])), 'admin');
    
    // Redirect to the Error page, with the error code
    $url = url('cart/constriv/'.$order_id.'/error/' . $hashid . '/' . $data['responsecode'], array('absolute' => TRUE));
  }

  // Output the REDIRECT= message
  echo 'REDIRECT=' . $url;
  die();
}

/**
 * Page for the ErrorURL or /cart/constriv/%/error/%
 */
function ubercart_constriv_redirect_error($order_id, $hashid = null, $errorcode = null) {
  $order = uc_order_load($order_id);
  
  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    watchdog('constriv', 'Error in ErrorURL for order #!orderid: order is not in CheckOut status.',
      array('!orderid' => $order_id), WATCHDOG_ERROR);
    drupal_set_message(t('An error has occurred during payment process. Please contact us to ensure your order has been submitted.'), 'error');
    drupal_goto('cart');
  }
  
  if ($hashid == null) {
    // We arrived here from the ErrorURL
    watchdog('constriv', 'Error in ErrorURL for order #!orderid: *not implemented yet*.',
      array('!orderid' => $order_id), WATCHDOG_ERROR);
    drupal_set_message(t('An error has occurred during payment process. Please contact us to ensure your order has been submitted.'), 'error');
    drupal_goto('cart');
  }
  else {
    // We get redirected here from a something wrong with the Payment
    
    if ($hashid != ubercart_constriv_get_hashid($order_id)) {
      // We get here from with a wrong HASH-ID
      watchdog('constriv', 'Error in ErrorURL for order #!orderid: unmatching HASH-ID received.',
        array('!orderid' => $order_id), WATCHDOG_ERROR);
      drupal_goto('cart');
    }
    else {
      watchdog('constriv', 'Error in ErrorURL for order #!orderid: error code received "!errorcode".',
        array('!orderid' => $order_id, '!errorcode' => $errorcode), WATCHDOG_ERROR);
      drupal_set_message(t('An error has occurred during payment process. Please contact us to ensure your order has been submitted.'), 'error');
      drupal_goto('cart');
    }
    
  }
  
}

/**
 * Returns the server data from the given variable
 */
function _ubercart_constriv_parse_server_data($var) {
  return array(
    'paymentid' =>    $var['paymentid'],
    'tranid' =>       $var['tranid'],
    'result' =>       $var['result'], // APPROVED, NOT APPROVED, CAPTURED, NOT CAPTURED
    'auth' =>         $var['auth'],
    'postdate' =>     $var['postdate'], // date format: mmgg
    'trackid' =>      $var['trackid'], // The OrderID
    'ref' =>          $var['ref'],
    'responsecode' => $var['responsecode'],
    'cardtype' =>     $var['cardtype'], // VISA, MC, AMEX, DINERS, JCB
    'payinst' =>      $var['payints'],
    'liability' =>    $var['liability'], // Y, N
    'udf1' =>         $var['udf1'],
    'udf2' =>         $var['udf2'],
    'udf3' =>         $var['udf3'],
    'udf4' =>         $var['udf4'], // Our check-value
    'udf5' =>         $var['udf5'],
    // If a technical error occurred, those 2 fields are filled
    'error' =>        $var['Error'],
    'errortext' =>    $var['ErrorText'],
  );
}

/**
 * Returns the textual description for the given Error-Code
 */
function _ubercart_constriv_get_server_error_codes($errorcode) {
  return '';
}